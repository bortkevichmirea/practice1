import psycopg2
import time

time.sleep(20)

conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="172.19.0.2",
    port="5432"
)

cur = conn.cursor()

cur.execute("""
    SELECT DISTINCT Teachers.teacher_id, Teachers.first_name, Teachers.last_name 
    FROM Teachers 
    INNER JOIN RecordBooks ON Teachers.teacher_id = RecordBooks.teacher_id
    WHERE RecordBooks.student_id = 3
""")

rows = cur.fetchall()

with open('result.txt', 'w') as file:
    file.write("ID\tИмя\tФамилия\n")
    for row in rows:
        file.write(f"{row[0]}\t{row[1]}\t{row[2]}\n")

cur.close()
conn.close()
